module.exports = {
  mode: 'jit',
  purge: ['./src/pages/**/*.tsx', './src/components/**/*.tsx'],
  darkMode: false,
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
    colors: {
      white: "#ffffff",
      red: "red",
      gray: {
        '50':  '#f9fafb',
        '100': '#f4f5f7',
        '200': '#e5e7eb',
        '300': '#d2d6dc',
        '400': '#9fa6b2',
        '500': '#6b7280',
        '600': '#4b5563',
        '700': '#374151',
        '800': '#252f3f',
        '900': '#161e2e',
      },
      cerise: {
        '50':  '#fdfcfb',
        '100': '#faeff2',
        '200': '#f6cbe6',
        '300': '#ec9ecb',
        '400': '#ea6fac',
        '500': '#df4b91',
        '600': '#c93270',
        '700': '#a22652',
        '800': '#771b35',
        '900': '#49111c',
      }
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
