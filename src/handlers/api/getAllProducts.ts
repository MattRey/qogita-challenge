import {ProductsResponse} from '../../types';

const getAllProducts = async (page: string): Promise<ProductsResponse | null> => {
  const response = await fetch(`http://localhost:3000/api/products?page=${page}`);
  if (response.ok) {
    return response.json();
  } else {
    throw new Error(`Request failed - ${response.status} - ${response.statusText}`);
  }
}

export default getAllProducts;