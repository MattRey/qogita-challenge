import {Product} from '../../types';

const getSingleProduct = async (gtin: string): Promise<Product | null> => {
  const response = await fetch(`http://localhost:3000/api/products/${gtin}`);
  if (response.ok) {
    return response.json();
  } else {
    throw new Error(`Request failed - ${response.status} - ${response.statusText}`);
  }
}

export default getSingleProduct;