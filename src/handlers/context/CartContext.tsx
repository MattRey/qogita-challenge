import React, { useState } from 'react';
import { Product } from '../../types';

interface CartCount {
  count: number;
}

interface CartContext {
  cart: CartProduct[] | [];
  updateCart: (cart: CartProduct[] | []) => void;
}

export type CartProduct = Product & CartCount;

type Props = {
  children: React.ReactNode;
};

export const CurrentCartContext = React.createContext<CartContext>({
  cart: [],
  updateCart: (cart: CartProduct[] | []) => {},
});

const CartContext = ({children}: Props) => {
  const [cart, updateCart] = useState<CartProduct[] | []>([]);
  return (
    <CurrentCartContext.Provider value={{cart, updateCart}}>
      {children}
    </CurrentCartContext.Provider>
  );
};

export default CartContext;
