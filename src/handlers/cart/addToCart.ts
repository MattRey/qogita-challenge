import { MouseEvent, useContext } from "react";
import { Product } from "../../types";
import { CartProduct, CurrentCartContext } from "../context/CartContext";

const addToCart = (payload: Product, cart: CartProduct[]) => {
  if (cart.every((product) => product.gtin !== payload.gtin)) {
    const updatedSingleCart = [...cart];
    const payloadWithCount = {...payload, count: 1};
    updatedSingleCart.push(payloadWithCount as CartProduct)
    return updatedSingleCart;
  } else {
    const updatedMultiCart = [...cart];
    cart.forEach((product, index) => {
      if(product.gtin === payload.gtin) {
        updatedMultiCart[index].count += 1;
      }
    })
    return updatedMultiCart;
  }
}

export default addToCart;