import Link from 'next/link';
import React, { useContext } from 'react';
import { CurrentCartContext } from '../../handlers/context/CartContext';
import CartCounter from '../../components/UI/atoms/CartCounter';
import { useRouter } from 'next/router';

type Props = {
  children: React.ReactNode;
};

const Layout = ({ children }: Props) => {
  const {cart} = useContext(CurrentCartContext);
  const router = useRouter();
  const cartCount = cart.length ? cart.map((item) => item.count).reduce((a,b) => a+b) : 0;
  return (
    <div className="container mx-auto px-4">
      <div className="flex justify-between w-full mb-12 py-6">
        <strong>Qogita</strong>
        <nav>
          <ul className="flex gap-4">
            <li>
              <Link href="/products/1">
                <a className={`text-gray-500 hover:text-red ${router.pathname === "/products/[page]" ? "text-red" : ""}`}>Products</a>
              </Link>
            </li>
            <li>
              <Link href="/cart">
                <a className="">
                  <div className="relative">
                    <span className={`pl-5 text-gray-500 hover:text-red ${router.pathname === "/cart" ? "text-red" : ""}`}>Your Cart</span>

                    {/* Adds the cart counter indicator if items have been added to the cart: */}
                    {cart.length >= 1 && 
                    <div className="absolute bottom-1 left-0">
                      <CartCounter length={cartCount} />
                    </div>
                    }
                  </div>
                </a>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
        {children}
    </div>
  )
};

export default Layout;
