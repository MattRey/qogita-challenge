import React from 'react';
import { Product } from '../../../types';

type HeaderProps = Pick<Product, "name" | "imageUrl" | "brandName" | "categoryName">

const Header = (props: HeaderProps) => {
  const {name, imageUrl, brandName, categoryName} = props;

  return (
    <div className="relative w-full h-96">
      <img src={imageUrl} alt={name} className="w-full h-full object-cover" />
      <div className="absolute bottom-0 left-0 w-full h-48 p-2 bg-gradient-to-t from-gray-700">
        <div className="absolute bottom-10 left-10 ">
          <h1 className="text-xl text-gray-50">Product: {name}</h1>
          <h3 className="text-md text-gray-200">Brand: {brandName}</h3>
          <h3 className="text-md text-gray-200">Category: {categoryName}</h3>
        </div>
      </div>
      
    </div>
  );
};

export default Header;