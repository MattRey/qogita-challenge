import Link from 'next/link';
import React from 'react';

interface PaginationProps {
  count: number;
  page: number;
}

const Pagination = (props: PaginationProps) => {
  const {count, page} = props;
  const pageArray = Array.from(Array(count), (e,i) => i + 1);
  return (
    <div className="flex justify-center w-full h-6 my-12">
      <div className="flex">
        {pageArray.map((pageLink) => <Link href={`/products/${pageLink}`} key={pageLink}><a className=""><div className={`w-6 h-6 mr-2 rounded-full text-center text-gray-50 ${pageLink === page ? "bg-red" : "bg-gray-500"}`}>{pageLink}</div></a></Link>)}
      </div>
    </div>
  );
};

export default Pagination;