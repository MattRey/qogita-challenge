import React, { useContext } from 'react';
import Link from 'next/link';
import { CurrentCartContext } from '../../../handlers/context/CartContext';
import { Product } from '../../../types';
import addToCart from '../../../handlers/cart/addToCart';

interface ProductCardProps {
  payload: Product;
}

export const ProductCard = (props: ProductCardProps): JSX.Element => {
  const {name, gtin, recommendedRetailPrice, recommendedRetailPriceCurrency, imageUrl, brandName, categoryName} = props.payload;

  const {cart, updateCart} = useContext(CurrentCartContext);

  const handleItem = () => {
    const newCart = addToCart(props.payload, cart);
    updateCart(newCart)
  }

  return (
    <article className="grid grid-cols-1 place-content-between h-full p-0 rounded-md">
      <Link href={`/product/${gtin}`}>
        <a className="">
          <div className="relative">
            <img src={imageUrl} alt={name} className="h-full w-full object-cover rounded-md" />
            <div className="absolute bottom-0 left-0 w-full h-36 p-2 bg-gradient-to-t from-gray-600 rounded-md">
              <h3 className="absolute bottom-4 right-4 text-lg text-gray-50">{recommendedRetailPriceCurrency === "EUR" ? "€" : "£"} {recommendedRetailPrice}</h3>
            </div>
          </div>
          <div className="mt-4">
            <div>
              <h2 className="w-full mt-2 text-sm text-gray-600 font-semibold">{name}</h2>
            </div>
          </div>
        </a>
      </Link>
      <div className="mt-2">
        <h3 className="mt-2 text-xs text-gray-500 font-medium">{categoryName}</h3>
        <button onClick={handleItem} className="w-full p-2 mt-4 rounded-md bg-gray-200 hover:bg-gray-500 text-sm text-gray-700 hover:text-gray-50 transition-colors duration-300">Add to cart</button>
      </div>
    </article>
  );
};
