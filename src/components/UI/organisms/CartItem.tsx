import Link from 'next/link';
import React, { MouseEvent, useContext } from 'react';
import { CartProduct, CurrentCartContext } from '../../../handlers/context/CartContext';

interface CartItemProps {
  payload: CartProduct;
}

const CartItem = (props: CartItemProps) => {
  const {name, gtin, count, recommendedRetailPrice,imageUrl} = props.payload;

  const {cart, updateCart} = useContext(CurrentCartContext);

  const amendCartItem = (e: MouseEvent) => {
    const updatedCart = [...cart]
    switch ((e.target as HTMLButtonElement).value) {
      case "inc":
        cart.forEach((product, index) => {
          if(product.gtin === gtin) {
            updatedCart[index].count += 1;
          }
        })
        updateCart(updatedCart)
        break;
      case "dec":
        cart.forEach((product, index) => {
          if(product.gtin === gtin && product.count > 1) {
            updatedCart[index].count -= 1;
          } else if(product.gtin === gtin && product.count === 1) {
            updatedCart.splice(index, 1)
          }
        })
        updateCart(updatedCart)
        break;
      case "rem":
        cart.forEach((product, index) => {
          if(product.gtin === gtin && product.count) {
            updatedCart.splice(index, 1)
          }
        })
        updateCart(updatedCart)
        break;
      default:
        break;
    }
  }

  return (
    <div className="flex items-center justify-between w-full py-3 border-b-2">
      <div className="">
        <Link href={`/product/${gtin}`}>
          <a className="flex items-center">
            <img src={imageUrl} alt={name} className="w-12 h-12 rounded-full border-2" />
            <h4 className="text-gray-400 text-base ml-6">{name}</h4>
          </a>
        </Link>
      </div>
      <div className="flex items-center justify-between w-48">
        <h4 className="text-gray-400 text-base">Quantity: {count}</h4>
        <button value="inc" onClick={amendCartItem} className="w-6 h-6 bg-gray-300 hover:bg-gray-600 hover:text-gray-50 rounded-full">+</button>
        <button value="dec" onClick={amendCartItem} className="w-6 h-6 bg-gray-300 hover:bg-gray-600 hover:text-gray-50 rounded-full">-</button>
        <button value="rem" onClick={amendCartItem} className="text-xs text-red">Remove</button>
      </div>
    </div>
  );
};

export default CartItem;