import React from 'react';

interface CartCounterProps {
  length: number;
}

const CartCounter = ({length}: CartCounterProps) => {
  return (
    <span className="flex m-auto h-4 w-4 mt-1">
      <span className="animate-ping absolute inline-flex h-4 w-4 rounded-full bg-red opacity-75"></span>
      <span className="relative inline-flex justify-center rounded-full h-4 w-4 bg-red text-white font-medium text-xs">{length}</span>
    </span>
  );
};

export default CartCounter;