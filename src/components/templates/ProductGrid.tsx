import React from "react";
import { Product } from "../../types";
import { ProductCard } from "../UI/organisms/ProductCard";

interface ProductGridProps {
  payload: Product[];
}

const ProductGrid = (props: ProductGridProps): JSX.Element => {
  const {payload} = props;
  return (
    <section id="productGrid" className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-10">
      {payload.map((product: Product) => <ProductCard key={product.gtin} payload={product} />)}
    </section>
  )
}

export default ProductGrid;