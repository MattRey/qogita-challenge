import type { AppProps } from 'next/app';

import '../global.css';
import CartContext from '../handlers/context/CartContext';

const QogitaApp = ({ Component, pageProps }: AppProps): JSX.Element => (
  <>
    <CartContext>
      <Component {...pageProps} />
    </CartContext>
  </>
);

export default QogitaApp;
