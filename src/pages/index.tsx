import React, { useEffect, useState } from 'react';
import Layout from '../components/Layout';
import {ProductsResponse} from '../types';
import getAllProducts from '../handlers/api/getAllProducts';
import ProductGrid from '../components/templates/ProductGrid';
import Pagination from '../components/UI/molecules/Pagination';
import { useRouter } from 'next/router';

const HomePage = () => {

  const [data, updateData] = useState<ProductsResponse | null>(null);

  const router = useRouter();
  
  useEffect(() => {

    if(!router.isReady) return;

    // IIFE to populate product data object on mount
    (async function loadHomeProducts() {
      const allProductsResponse = await getAllProducts("1");
      updateData(allProductsResponse);
    })();
    
  }, [router.isReady]);

  return (
    <Layout>
      <h1 className="text-5xl text-gray-600 font-light mb-6">Products</h1>
      {data && 
      <>
        <ProductGrid payload={data.results} />
        <Pagination count={Math.round(data.count/20)} page={data.page} />
      </>
      }      
    </Layout>
  )
};

export default HomePage;
