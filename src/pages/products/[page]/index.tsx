import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../../components/Layout';
import {ProductsResponse} from '../../../types';
import getAllProducts from '../../../handlers/api/getAllProducts';
import ProductGrid from '../../../components/templates/ProductGrid';
import Pagination from '../../../components/UI/molecules/Pagination';

const ProductsPage = (): JSX.Element => {
  const [data, updateData] = useState<ProductsResponse | null>(null);

  const router = useRouter();
  const { page } = router.query;
  
  useEffect(() => {

    if(!router.isReady) return;

    const pageNum = page as string;

    // IIFE to populate product data object on mount
    (async function loadHomeProducts() {
      const allProductsResponse = await getAllProducts(pageNum);
      updateData(allProductsResponse);
    })();
    
  }, [page, router.isReady]);

  return (
    <Layout>
      <h1 className="text-5xl text-gray-600 font-light mb-12">Products</h1>
      {data && 
        <>
          <ProductGrid payload={data.results} />
          <Pagination count={Math.round(data.count/20)} page={data.page} />
        </>
      }
    </Layout>
  )
};

export default ProductsPage;
