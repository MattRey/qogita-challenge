import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import Layout from '../../../components/Layout';
import getSingleProduct from '../../../handlers/api/getSingleProduct';
import { ProductResponse } from '../../../types';
import Header from '../../../components/UI/molecules/Header';
import { CurrentCartContext } from '../../../handlers/context/CartContext';
import addToCart from '../../../handlers/cart/addToCart';

const Product = (): JSX.Element => {

  const router = useRouter();
  const { gtin } = router.query;

  const [data, updateData] = useState<ProductResponse | null>(null);

  useEffect(() => {
    if(!router.isReady) return;

    const apiParam = gtin as string;

    // IIFE to populate product data object on mount
    (async function loadHomeProducts() {
      const singleProductResponse = await getSingleProduct(apiParam);
      updateData(singleProductResponse);
    })();
  }, [gtin, router.isReady]);

  const {cart, updateCart} = useContext(CurrentCartContext);

  const handleItem = () => {
    if (data){
      const newCart = addToCart(data, cart)
      updateCart(newCart)
    };
  }
  
  return (
    <Layout>
      <div>
        {data &&
        <>
          <Header 
            name={data.name} 
            imageUrl={data.imageUrl} 
            brandName={data.brandName} 
            categoryName={data.categoryName} 
          />
          <div className="mt-2">
            <h3 className="mt-2 text-xl text-gray-500 font-medium">{data.recommendedRetailPriceCurrency === "EUR" ? "€" : "£"} {data.recommendedRetailPrice}</h3>
            <button onClick={handleItem} className="w-60 p-2 mt-4 rounded-md bg-gray-200 hover:bg-gray-500 text-sm text-gray-700 hover:text-gray-50 transition-colors duration-300">Add to cart</button>
          </div>
        </>
        }
      </div>
    </Layout>
  );
};

export default Product;