import { useContext } from 'react';
import Layout from '../../components/Layout';
import CartContext, { CurrentCartContext } from '../../handlers/context/CartContext';
import CartItem from '../../components/UI/organisms/CartItem';

const CartPage = (): JSX.Element => {
  const {cart} = useContext<CartContext>(CurrentCartContext);
  const totalAmount = cart.map((item) => item.count * item.recommendedRetailPrice).reduce((a,b) => a+b, 0);
  return (
    <Layout>
      <h1 className="text-5xl text-gray-600 font-light mb-6">Your Cart</h1>
      <div>
        {cart.length > 0 ? cart.map((item) => <CartItem key={item.gtin} payload={item} />) : <h3>There is nothing in your cart, please buy something.</h3>}
      </div>
      <h1 className="mt-12 text-3xl text-gray-600">Total: €{totalAmount.toFixed(2)}</h1>
    </Layout>
  )
};

export default CartPage;
